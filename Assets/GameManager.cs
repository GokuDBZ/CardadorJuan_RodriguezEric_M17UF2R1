using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject player;
    public GameObject gameOver;
    public GameObject win;
    public GameObject mainMenu;
    public GameObject HUD;
    public GameObject PlayerHealth;
    public GameObject PlayerMana;
    public TextMeshProUGUI PowerPotionCounter;
    public TextMeshProUGUI CoinCounter;
    public float HealthBarStartPosition;
    public float ManaBarStartPosition;
    public float difficulty;

    public string actualRoom;

    public bool isDead = true;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
            SceneManager.LoadScene(0);
        }
        else if (instance != this) Destroy(gameObject);

    }
    public void Start()
    {
        difficulty = 1;
        gameOver.SetActive(false);
        isDead = true;
        SoundManager.instance.Play("MainMenu");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && SceneManager.GetActiveScene().buildIndex != 0) SceneChange(0);

        if(HealthBarStartPosition == 0 && GameObject.Find("HealthBar") != null)
        {
            HealthBarStartPosition = GameObject.Find("HealthBar").transform.position.x;
        }
        if (ManaBarStartPosition == 0 && GameObject.Find("ManaBar") != null)
        {
            ManaBarStartPosition = GameObject.Find("ManaBar").transform.position.y;
        }
    }

    public void SceneChange(int scene)
    {
        switch (scene)
        {
            case 0:
                SoundManager.instance.Stop("music");
                gameOver.SetActive(false);
                HUD.SetActive(false);
                mainMenu.SetActive(true);
                ResetValues();
                Time.timeScale = 0;
                SceneManager.LoadScene(0);
                SoundManager.instance.Play("MainMenu");
                break;
            case 1:
                SoundManager.instance.Stop("MainMenu");
                SoundManager.instance.Play("music");
                SceneManager.LoadScene(1);
                mainMenu.SetActive(false);
                gameOver.SetActive(false);
                HUD.SetActive(true);
                ResetValues();
                break;
            case 2:
                SoundManager.instance.Stop("MainMenu");
                SoundManager.instance.Play("music");
                SceneManager.LoadScene(2);
                mainMenu.SetActive(false);
                gameOver.SetActive(false);
                HUD.SetActive(true);
                ResetValues();
                break;
            default:
                break;
        }
    }
    public void CloseGame()
    {
        Application.Quit();
    }
    public void ResetValues()
    {
        ResetConsumables();
        Time.timeScale = 1;
        isDead = false;
        gameOver.SetActive(false);
        SoundManager.instance.Stop("GameOver");
    }
    public void Death()
    {
        SoundManager.instance.Play("GameOver");
        SoundManager.instance.Stop("music");
        gameOver.SetActive(true);
        isDead = true;
        Time.timeScale = 0;
        difficulty = 1;
    }
    public bool PowerUp(bool consume)
    {
        if(consume)
        {
            if(Convert.ToInt32(PowerPotionCounter.text) > 0)
            {
                PowerPotionCounter.text = (Convert.ToInt32(PowerPotionCounter.text) - 1).ToString();
                return true;
            }
            return false;
        }
        PowerPotionCounter.text = (Convert.ToInt32(PowerPotionCounter.text) + 1).ToString();
        return false;
    }
    public bool Coins(int value, bool consume)
    {
        if (consume)
        {
            if (Convert.ToInt32(CoinCounter.text) >= value)
            {
                CoinCounter.text = (Convert.ToInt32(CoinCounter.text) - value).ToString();
                return true;
            }
            return false;
        }
        CoinCounter.text = (Convert.ToInt32(CoinCounter.text) + value).ToString();
        return false;
    }
    private void ResetConsumables()
    {
        CoinCounter.text = "50";
        PowerPotionCounter.text = "0";
    }
    public void NewGamePlus()
    {
        difficulty += 0.2f;
        SceneChange(1);
    }
}