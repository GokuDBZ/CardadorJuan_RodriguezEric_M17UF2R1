using TMPro;
using UnityEngine;

public class Altar : Entity
{
    [SerializeField] private GameObject itemPreFab;
    private Sprite withItem;
    [SerializeField] private Sprite withoutItem;
    private TextMeshPro priceText;
    [SerializeField] private bool reusable;
    public float itemTakeTime;
    public bool restocking;

    public GameObject item;

    void Start()
    {
        priceText = transform.Find("PriceText").GetComponent<TextMeshPro>();
        withItem = sr.sprite;
        restocking = false;
        PutItem();
    }
    private void Update()
    {
        if(!restocking)
        {
            if (item == null)
            {
                if (reusable)
                {
                    itemTakeTime = Time.time;
                    restocking = true;
                }
                sr.sprite = withoutItem;
                priceText.text = "";
            }
        }
        if(item != null && item.TryGetComponent(out Consumable con))
        {
            con.dropTime = Time.time;
        }
        if(Time.time - itemTakeTime > 2 && reusable && restocking)
        {
            PutItem();
            restocking = false;
        }
    }
    private void PutItem()
    {
        item = Instantiate(itemPreFab, transform);
        item.transform.position = new Vector3(item.transform.position.x, item.transform.position.y + 0.13f, -2);
        item.transform.localScale = new Vector3(0.15f, 0.15f, 1);
        if (item.TryGetComponent(out Recollectable rec)) priceText.text = rec.price.ToString() + "€";
        if (item.TryGetComponent(out Consumable con))
        {
            priceText.text = con.price.ToString() + "€";
            item.transform.localScale = new Vector3(1f, 1f, 1);
            item.transform.position += new Vector3( 0, 0.25f, 0);
        }
        sr.sprite = withItem;
    }
}
