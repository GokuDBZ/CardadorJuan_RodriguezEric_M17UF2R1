using System;
using UnityEngine;
using System.Linq;

public class Enemy : Mob
{
    public EnemySO enemySO;
    private bool triggered;
    private Vector3 playerPosition;
    private float startTime;
    private float clock;
    private float clockSec;
    public Vector2 vector;
    public float onKnockback;
    public float changeDirection;
    public float damageColor;

    new private void Start()
    {
        base.Start();

        anim.runtimeAnimatorController = enemySO.animatorController;
        maxHealth = enemySO.HP * GameManager.instance.difficulty;
        health = maxHealth;
        sr.sprite = enemySO.sprite;
        WeaponsInventory.Add(enemySO.weapon);
        speed = enemySO.speed;
        startTime = Time.time;
        baseDamage = GameManager.instance.difficulty;
        damageColor = 0;

        triggered = false;
        System.Random rnd = new();
        degrees = (float)(rnd.NextDouble() * (1 - -4) + -4);
    }
    private void Update()
    {
        UpdateHealth();

        if (Time.time - startTime > 1f)
        {
            if (enemySO.behaviour.ToString() == "Kamikaze") anim.SetBool("Moving", triggered);
            Action();

            if (Time.time - clockSec > 1f)
            {
                if(dot.Select(item => item[0]).Sum() > 0)
                {
                    TakeDamage(dot.Select(item => item[0]).Sum());
                }
                for (int i = 0; i < dot.Count; i++)
                {
                    dot[i] = new Vector2(dot[i][0], dot[i][1] - 1);
                }
                dot = dot.Where(item => item[1] > 0).ToList();
                clockSec = Time.time;
            }

            if (Time.time - clock > 0.1f)
            {
                if(damageColor >= 0)
                {
                    sr.color = new Color(255, 255 - 204 * damageColor * 2, 255  - 255 * damageColor * 2);
                    if (damageColor >= 0.5)
                    {
                        sr.color = Color.red;
                    }
                    damageColor -= 0.1f;
                }
                if (enemySO.HealthRegen)
                {
                    health += 10;
                    if (health > maxHealth) health = maxHealth;
                }
                if (anim.GetFloat("Attack") > 0)
                    anim.SetFloat("Attack", anim.GetFloat("Attack") - 1);
                clock = Time.time;

                if(vector != Vector2.zero)
                {
                    rb.velocity = Vector2.zero;
                    GetComponent<Rigidbody2D>().AddForce(vector);
                    vector -= vector * 0.01f;
                }

                if (changeDirection > 0) changeDirection--;

                if (onKnockback > 0) onKnockback--;
            }
            else triggered = false;

            Action();
        }
    }
    private void UpdateHealth()
    {
        healthText.text = health.ToString();
        healthBar.GetComponent<SpriteRenderer>().size = new Vector2(0.5f * health / maxHealth, healthBar.GetComponent<SpriteRenderer>().size.y);

        if (health / maxHealth > 0.5f) healthBar.GetComponent<SpriteRenderer>().color = Color.green;
        if (health / maxHealth < 0.5f && health / maxHealth > 0.25f) healthBar.GetComponent<SpriteRenderer>().color = Color.yellow;
        if (health / maxHealth < 0.25f) healthBar.GetComponent<SpriteRenderer>().color = Color.red;
    }
    private void Action()
    {
        Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, enemySO.range).Where(col => col.CompareTag("Player")).ToArray();

        triggered = cols.Count() > 0;

        if (triggered && anim.GetFloat("Attack") == 0)
        {
            playerPosition = cols[0].transform.position - transform.position;
            float rad = Mathf.Atan2(playerPosition.y, playerPosition.x) - Mathf.PI / 2;
            firePoint.transform.rotation = Quaternion.Euler(0, 0, rad * Mathf.Rad2Deg);
            degrees = rad;
            anim.SetFloat("Degrees", degrees);
            firePoint.transform.position = new Vector2(transform.position.x + -0.5f * Mathf.Sin(rad), transform.position.y + 1f * Mathf.Cos(rad));

            switch (enemySO.behaviour)
            {
                case EnemySO.Behaviour.Turret:

                    if (WeaponsInventory[GetWeaponEquipped()].Shoot(firePoint.GetComponent<Transform>(), tag, rad, lastShoot, baseDamage)) 
                    { 
                        lastShoot = Time.time;
                        anim.SetFloat("Attack", WeaponsInventory[GetWeaponEquipped()].cooldown*10);
                        WeaponsInventory[GetWeaponEquipped()].PlayWeaponSound();
                        if (onKnockback == 0) rb.velocity = Vector2.zero;
                    }
                    break;
                case EnemySO.Behaviour.Kamikaze:
                    foreach (var item in Physics2D.OverlapCircleAll(transform.position, 0.5f))
                    {
                        if(item.TryGetComponent(out Player player))
                        {
                            anim.SetFloat("Attack", 10f);
                            SoundManager.instance.Play("Kamikaze");
                            player.TakeDamage(enemySO.contactDamage);
                            player.GetComponent<PlayerCollisions>().Knockback(10, transform.position);
                        }
                    }
                    if (onKnockback == 0)
                    {
                        if(triggered)
                        {
                            if (Math.Abs(playerPosition.x) > Math.Abs(playerPosition.y))
                            {
                                if (playerPosition.x > 0) rb.velocity = new Vector2(speed, 0);
                                if (playerPosition.x < 0) rb.velocity = new Vector2(-speed, 0);
                            }
                            else
                            {
                                if (playerPosition.y > 0) rb.velocity = new Vector2(0, speed);
                                if (playerPosition.y < 0) rb.velocity = new Vector2(0, -speed);
                            }
                        }
                    }
                    break;
                default:
                    break;
            }

            return;
        }
        else if (changeDirection <= 0 && onKnockback == 0)
        {
            vector = Vector2.zero;
            System.Random rnd = new();

            changeDirection = (float)(rnd.NextDouble() * (3 - 1) + 1);
            int direction = 1, invers = 1;
            if (UnityEngine.Random.Range(-1, 1) < 0) direction = -1;
            if (UnityEngine.Random.Range(-1, 1) < 0) invers = -1;

            switch(direction)
            {
                case -1:
                    rb.velocity = new Vector2(0, speed * invers);
                    break;
                case 1:
                    rb.velocity = new Vector2(speed * invers, 0);
                    break;
            }
        }
        return;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        switch (col.tag)
        {
            case "Projectile_Player":
                TakeDamage(col.GetComponent<Projectile>().damage);
                DamageOverTime(col.GetComponent<Projectile>().dot);
                if (!enemySO.HealthRegen)
                {
                    vector = 10 * col.transform.GetComponent<Projectile>().knockback * (transform.position - col.transform.position);
                    onKnockback = 5;
                }
                break;
        }
    }
    override public void Death()
    {
        foreach (Drop item in enemySO.drops)
        {
            TryDrop(item);
        }
        if (transform.parent.GetComponent<EnemyController>().TryKey(out GameObject key))
        {
            TryDrop(new Drop("Key", key, 1f));
        }
        base.Death();
        SoundManager.instance.Play("EnemyDeath");
    }
    private void TryDrop(Drop drop)
    {
        System.Random rnd = new();
        double num = rnd.NextDouble();
        if (num < drop.rate)
        {
            GameObject dropGO = Instantiate(drop.obj, transform.parent);
            dropGO.transform.position = transform.position;
            dropGO.GetComponent<Rigidbody2D>().velocity = new Vector2((float)(4 * rnd.NextDouble() * (1 - -1) + -1), (float)(4 * rnd.NextDouble() * (1 - -1) + -1));
        }
    }
    override public void TakeDamage(float value)
    {
        base.TakeDamage(value);
        damageColor = 1;
    }
}
