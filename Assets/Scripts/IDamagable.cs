public interface IDamagable
{
    float health { get; set; }
    float maxHealth { get; set; }
    void Heal(float value);
    void TakeDamage(float value);
    void DamageOverTime(UnityEngine.Vector2 dot);
    void Death();
}
