using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using System.Linq;

public class Player : Mob
{
    private Vector2 movement;
    public bool loopingSound;
    [SerializeField] private InputActionReference press;
    private float maxMana;
    public float mana;
    private float manaRegeneration;
    private float lastManaRegeneration;
    private float lastHealthRegeneration;
    private float clock;
    public float powerUp;
    public float iframes;

    private new void Start()
    {
        name = "Player";
        base.Start();
        press.action.performed += EquipWeapon;

        maxHealth = 100;
        maxMana = 100;
        health = maxHealth;
        lastHealthRegeneration = Time.time;

        mana = maxMana;
        manaRegeneration = 10;
        lastManaRegeneration = Time.time;
        clock = Time.time;

        powerUp = 0;
        speed = 5f;
        GameObject.FindGameObjectWithTag("MainCamera").transform.position = new Vector3(transform.position.x, transform.position.y, -10);
    }

    private void Update()
    {
        if(!GameManager.instance.isDead)
        {
            if (Input.GetKeyDown(KeyCode.Mouse1) && GameManager.instance.PowerUp(true)) PowerUp();

            baseDamage = 1 + 0.5f * System.Convert.ToInt32(powerUp > 0);

            if (Time.time - clock > 0.1f)
            {
                if (powerUp > 0) powerUp--;
                if (iframes > 0) iframes--;

                if (anim.GetFloat("Hurt") > 0)
                    anim.SetFloat("Hurt", anim.GetFloat("Hurt") - 1);
                clock = Time.time;
            }

            anim.SetBool("Moving", movement != Vector2.zero);
            rb.MovePosition(rb.position + speed * Time.fixedDeltaTime * movement);

            var mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mouse -= transform.position;
            float rad = Mathf.Atan2(mouse.y, mouse.x) - Mathf.PI / 2;
            firePoint.transform.rotation = Quaternion.Euler(0, 0, rad * Mathf.Rad2Deg);
            degrees = rad;
            anim.SetFloat("Degrees", degrees);
            firePoint.transform.position = new Vector2(transform.position.x + -0.5f * Mathf.Sin(rad), transform.position.y + 1f * Mathf.Cos(rad));

            UpdateHealth();
            UpdateMana();
            UpdateInventory();

            if (Input.GetKey(KeyCode.Mouse0))
            {
                if (WeaponsInventory.Count != 0 &&
                    WeaponsInventory[GetWeaponEquipped()].manaCost <= mana &&
                    WeaponsInventory[GetWeaponEquipped()].Shoot(firePoint.GetComponent<Transform>(), tag, rad, lastShoot, baseDamage))
                    {
                        lastShoot = Time.time;
                        WeaponsInventory[GetWeaponEquipped()].PlayWeaponSound(ref loopingSound);
                        mana -= WeaponsInventory[GetWeaponEquipped()].manaCost;
                    }
            }

            if (WeaponsInventory.Count > 0 &&
                SoundManager.instance.sounds.Find(s => s.name == WeaponsInventory[GetWeaponEquipped()].sound.ToString()).loop)
            {
                if (!Input.GetKey(KeyCode.Mouse0))
                {
                    WeaponsInventory[GetWeaponEquipped()].StopWeaponSound(ref loopingSound);
                }
            }
        }
    }
    private void UpdateHealth()
    {
        if (Time.time - lastHealthRegeneration > 1 && powerUp > 0)
        {
            health += 2; ;
            lastHealthRegeneration = Time.time;
        }
        if (health > maxHealth) health = maxHealth;

        healthBar = GameManager.instance.PlayerHealth.transform.Find("HealthBarMask").Find("HealthBar").gameObject;
        healthBorder = GameManager.instance.PlayerHealth.transform.Find("HealthBorder").gameObject;
        Transform healthText = GameManager.instance.PlayerHealth.transform.Find("HealthText");

        healthText.GetComponent<TextMeshProUGUI>().text = health.ToString();

        healthBar.GetComponent<RectTransform>().sizeDelta = new Vector2(251 * health / maxHealth, 16);

        if (health / maxHealth > 0.5f) healthBar.GetComponent<Image>().color = Color.green;
        if (health / maxHealth < 0.5f && health / maxHealth > 0.25f) healthBar.GetComponent<Image>().color = Color.yellow;
        if (health / maxHealth < 0.25f) healthBar.GetComponent<Image>().color = Color.red;
    }
    private void UpdateMana()
    {
        if (Time.time - lastManaRegeneration > 1 / manaRegeneration)
        {
            mana++;
            if (powerUp > 0) mana += 2;
            maxMana = 100 + 50 * System.Convert.ToInt32(powerUp > 0);
            lastManaRegeneration = Time.time;
        }
        if (mana > maxMana) mana = maxMana;

        Transform manaBar = GameManager.instance.PlayerMana.transform.Find("ManaBarMask").Find("ManaBar");
        manaBar.Find("ManaBarPower").GetComponent<Image>().enabled = powerUp > 0;
        Transform manaBorder = GameManager.instance.PlayerMana.transform.Find("ManaBorder");
        Transform manaText = GameManager.instance.PlayerMana.transform.Find("ManaText");

        manaText.GetComponent<TextMeshProUGUI>().text = mana.ToString();

        manaBar.transform.position = new Vector3(manaBorder.transform.position.x, -594 * (1 - mana / maxMana) + GameManager.instance.ManaBarStartPosition, -2);
    }
    private void UpdateInventory()
    {
        Transform[] slots = new Transform[4];
        for (int i = 0; i < slots.Length; i++)
        {
            slots[i] = GameManager.instance.HUD.GetComponentsInChildren<Transform>().Where(item => item.name == $"Slot {i + 1}").ToArray()[0];
        }

        for (int i = 0; i < WeaponsInventory.Count; i++)
        {
            slots[i].GetComponent<Image>().color = new Color(255, 255, 255, 1);
            if (i == weaponEquipped) slots[i].GetComponent<Image>().sprite = WeaponsInventory[i].highlightedSprite;
            else slots[i].GetComponent<Image>().sprite = WeaponsInventory[i].sprite;
        }
        for (int i = WeaponsInventory.Count; i < slots.Length; i++)
        {
            slots[i].GetComponent<Image>().color = new Color(0, 0, 0, 0);
            slots[i].GetComponent<Image>().sprite = null;
        }
    }
    private void EquipWeapon(InputAction.CallbackContext obj)
    {
        SetWeaponEquipped(GetWeaponEquipped() + (int)obj.ReadValue<float>());
    }
    public void PowerUp()
    {
        powerUp += 100;
        SoundManager.instance.Play("PowerUp");
    }
    public void OnMovement(InputValue value)
    {
        movement = value.Get<Vector2>();
    }
    override public void Death()
    {
        UpdateHealth();
        GameManager.instance.Death();
    }
    override public void TakeDamage(float value)
    {
        anim.SetFloat("Hurt", 5);
        if(iframes == 0)
        {
            iframes = 10;
            base.TakeDamage(value);
            SoundManager.instance.Play("Hurt");
        }
    }
}
