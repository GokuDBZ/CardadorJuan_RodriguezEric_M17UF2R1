using UnityEngine;

public class PlayerCollisions : MonoBehaviour
{
    private Vector2 vector;
    private float start;
    private void Update()
    {
        if(vector != Vector2.zero && Time.time - start > 0.1f)
        {
            GetComponent<Rigidbody2D>().AddForce(vector);
            vector -= vector * 0.01f;
        }
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        switch(col.tag)
        {
            case "Room":
                GameObject.Find("Main Camera").GetComponent<MainCamera>().MoveTo(col.transform.position);
                GameManager.instance.actualRoom = col.name;
                break;
            case "Recollectable":
                if(GameManager.instance.Coins(col.GetComponent<Recollectable>().price, true) && GetComponent<Player>().WeaponsInventory.Count < 4)
                {
                    GetComponent<Player>().AddWeapon(col.GetComponent<Recollectable>().Pick());
                }
                Knockback(5, col.transform.position);
                break;
            case "Consumable":
                if (GameManager.instance.Coins(col.GetComponent<Consumable>().price, true))
                {
                    col.GetComponent<Consumable>().Use(GetComponent<Player>());
                }
                if (col.GetComponent<Consumable>().price > 0)
                {
                    Knockback(5, col.transform.position);
                }
                break;
            case "EnemyController":
                col.GetComponent<EnemyController>().Spawn();
                break;
            case "Projectile_Enemy":
                GetComponent<Player>().TakeDamage(col.GetComponent<Projectile>().damage);
                Knockback(col.transform.GetComponent<Projectile>().knockback, col.transform.position);
                break;
        }
    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        switch (col.gameObject.tag)
        {
            case "Enemy":
                GetComponent<Player>().TakeDamage(col.gameObject.GetComponent<Enemy>().enemySO.contactDamage * col.gameObject.GetComponent<Enemy>().baseDamage);
                Knockback(col.transform.GetComponent<Enemy>().WeaponsInventory[0].knockback, col.transform.position);
                break;
            case "NewGame":
                Time.timeScale = 0;
                GameManager.instance.win.SetActive(true);
                break;
        }
    }
    public void Knockback(float value, Vector3 position)
    {
        vector = 10 * value * (transform.position - position);
        start = Time.time - 0.1f;
    }
}
