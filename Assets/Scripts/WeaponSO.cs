using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "WeaponSO", order = 2)]
public class WeaponSO : ScriptableObject
{
    public float speed;
    public float damage;
    public Vector2 dot;
    public float manaCost;
    public float lifeTime;
    public float dispersion;
    public float cooldown;
    public float scale;
    public float knockback;
    public bool destroyOnWall;
    public Sprite sprite;
    public Sprite highlightedSprite;
    public RuntimeAnimatorController bulletAnim;
    public GameObject projectilePreFab;
    public Sounds sound;

    //Clases
    public enum Sounds
    {
        Flamethrower,
        Sniper,
        Sniper1,
        Melee,
        Grenade,
        Turret,
        Kamikaze
    }
    public bool Shoot(Transform firePoint, string tag, float degrees, float lastShoot, float baseDamage)
    {
        if (Time.time - lastShoot > cooldown)
        {
            //SoundManager.instance.Play(sound.ToString());
            GameObject projectileGO = Instantiate(projectilePreFab, firePoint.position, firePoint.rotation);
            Destroy(projectileGO, lifeTime);
            Entity projectileEnt = projectileGO.GetComponent<Entity>();
            projectileEnt.transform.localScale = new Vector3(scale, scale, 0);
            SetSpeed(projectileEnt, degrees);
            projectileEnt.transform.Rotate(0,0,-90);
            projectileEnt.name = name + "_Projectile";
            projectileEnt.tag = "Projectile_" + tag;
            projectileEnt.anim.runtimeAnimatorController = bulletAnim;
            projectileEnt.GetComponent<Projectile>().destroyOnWall = destroyOnWall;
            projectileEnt.GetComponent<Projectile>().firePoint = firePoint;
            projectileEnt.GetComponent<Projectile>().damage = damage * baseDamage;
            projectileEnt.GetComponent<Projectile>().dot = dot;
            projectileEnt.GetComponent<Projectile>().knockback = knockback;
            return true;
        }
        return false;
    }
    void SetSpeed(Entity projectile, float rad)
    {
        rad = -rad;
        switch (name)
        {
            case "Melee":
                projectile.transform.Rotate(0,0,180);
                projectile.transform.localScale = new Vector2(2, 1) * scale;
                break;
            case "Sniper":
            case "GrenadeLauncher":
            case "Turret":
                projectile.rb.velocity = new Vector2(
                    Mathf.Sin(rad) * speed,
                    Mathf.Cos(rad) * speed
                );
                break;
            case "FlameThrower":
                projectile.rb.velocity = new Vector2(
                    Mathf.Sin(rad) * speed + (Mathf.Cos(rad) * Random.Range(-dispersion, dispersion)),
                    Mathf.Cos(rad) * speed + (Mathf.Sin(rad) * Random.Range(-dispersion, dispersion))
                );
                break;
        }
    }
    public void PlayWeaponSound(ref bool looping)
    {
        if (!looping)
        {
            SoundManager.instance.Play(sound.ToString());
            if (SoundManager.instance.sounds.Find(s => s.name == sound.ToString()).loop)
            {
                looping = true; 
            }
        }     
    }
    public void PlayWeaponSound()
    {
        SoundManager.instance.Play(sound.ToString());
    }
    public void StopWeaponSound(ref bool looping)
    {
        SoundManager.instance.Stop(sound.ToString());
        looping = false;
    }
}
