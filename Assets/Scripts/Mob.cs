using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Mob : Entity, IDamagable
{
    public GameObject firePoint;
    public List<WeaponSO> WeaponsInventory;
    public int weaponEquipped;
    public TextMeshPro healthText;
    public GameObject healthBar;
    public GameObject healthBorder;
    public float maxHealth { get; set; }
    public float health { get; set; }

    public float speed;
    [HideInInspector]
    public float degrees;
    [HideInInspector]
    public float lastShoot;
    public float baseDamage = 1;
    public List<Vector2> dot;

    public void Start()
    {
        degrees = 0;
        weaponEquipped = 0;
        lastShoot = 0;
    }
    public void SetWeaponEquipped(int value)
    {
        if (WeaponsInventory.Count == 0) return;
        if (value >= WeaponsInventory.Count) value -= WeaponsInventory.Count;
        if (value < 0) value += WeaponsInventory.Count;
        weaponEquipped = value;
    }
    virtual public int GetWeaponEquipped()
    {
        return weaponEquipped;
    }
    virtual public void AddWeapon(WeaponSO weapon)
    {
        WeaponsInventory.Add(weapon);
    }
    virtual public void Heal(float value)
    {
        health += value;
        if (health > maxHealth) health = maxHealth;
    }
    virtual public void TakeDamage(float value)
    {
        health -= value;
        if (health <= 0)
        {
            health = 0;
            Death();
        }
    }
    virtual public void DamageOverTime(Vector2 dot)
    {
        this.dot.Add(dot);
    }
    virtual public void Death()
    {
        Destroy(gameObject);
    }
}
