using UnityEngine;
using UnityEngine.SceneManagement;

public class MainCamera : MonoBehaviour
{
    private float timer;
    private Vector3 objPosition;
    private Vector3 basePosition;
    private GameObject player;
    private void Update()
    {
        if(SceneManager.GetActiveScene().buildIndex != 2)
        {
            if (Time.time - timer > 0.01f && !(transform.position.x == objPosition.x && transform.position.y == objPosition.y))
            {
                transform.position += (objPosition - basePosition) / 128;
                transform.position = new Vector3((float)System.Math.Round(transform.position.x, 2), (float)System.Math.Round(transform.position.y, 2), -10);
                if (Mathf.Abs(transform.position.y - objPosition.y) < 0.1f) transform.position = new Vector3(transform.position.x, (float)System.Math.Round(transform.position.y), -10);
                if (Mathf.Abs(transform.position.x - objPosition.x) < 0.1f) transform.position = new Vector3((float)System.Math.Round(transform.position.x), transform.position.y, -10);

                timer = Time.time;
            }
        }
        else
        {
            if(GameObject.Find("Player") == null)
            {
                player = Instantiate(GameManager.instance.player);
            }
            transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -10);
        }
    }
    public void MoveTo(Vector3 objPosition)
    {
        this.objPosition = objPosition;
        basePosition = transform.position;
    }
}
