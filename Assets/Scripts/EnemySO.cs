using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "EnemySO", order = 1)]
public class EnemySO : ScriptableObject
{
    //Atributos enemigo
    public RuntimeAnimatorController animatorController;
    public Sprite sprite;
    public float HP;
    public float speed;
    public float range;
    public WeaponSO weapon;
    public Behaviour behaviour;
    public float contactDamage;
    public bool HealthRegen;

    //Clases
    public enum Behaviour
    {
        Turret,
        Kamikaze
    }
    public List<Drop> drops = new();
}
[Serializable]
public class Drop
{
    public string name;
    public GameObject obj;
    [Range(0, 1)]
    public float rate;
    public Drop(string name, GameObject obj, float rate)
    {
        this.name = name;
        this.obj = obj;
        this.rate = rate;
    }
}
