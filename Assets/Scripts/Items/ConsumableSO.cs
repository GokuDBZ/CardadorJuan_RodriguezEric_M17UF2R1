using UnityEngine;
using System;

[CreateAssetMenu(fileName = "New Consumable", menuName = "ConsumableSO", order = 3)]
public class ConsumableSO : ScriptableObject
{
    public Sprite sprite;
    public int value;
    public Type type;
    public enum Type
    {
        HealthPotion,
        ManaPotion,
        PowerPotion,
        Key,
        Coin
    }
    public void Use(Player player)
    {
        switch (type)
        {
            case Type.HealthPotion:
                player.Heal(value);
                break;
            case Type.ManaPotion:
                player.mana += value;
                break;
            case Type.PowerPotion:
                GameManager.instance.PowerUp(false);
                break;
            case Type.Key:
                GameObject.Find("DungeonManager").GetComponent<DungeonManager>().OpenDoors(Convert.ToInt32(GameManager.instance.actualRoom.Split(' ')[1].Split(',')[0]), Convert.ToInt32(GameManager.instance.actualRoom.Split(' ')[2].Split(')')[0]), true);
                break;
            case Type.Coin:
                GameManager.instance.Coins(value, false);
                break;
            default:
                break;
        }
    }
}
