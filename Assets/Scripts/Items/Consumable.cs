using UnityEngine;
using System.Linq;

public class Consumable : Entity, ISerializationCallbackReceiver
{
    public ConsumableSO consum;
    public int price;
    public float dropTime;
    private void Start()
    {
        dropTime = Time.time;
    }
    private void Update()
    {
        rb.velocity -= rb.velocity * 0.01f;

        if (Time.time - dropTime > 2)
        {
            Collider2D col = Physics2D.OverlapCircleAll(transform.position, 4f).Where(item => item.CompareTag("Player")).ToList()[0];
            Vector2 magnet = new(col.transform.position.x - transform.position.x, col.transform.position.y - transform.position.y);
            rb.velocity += magnet * 0.2f;
        }
    }
    public void Use(Player player)
    {
        consum.Use(player);
        Destroy(gameObject);
    }
    public void OnBeforeSerialize()
    {
        GetComponent<SpriteRenderer>().sprite = consum.sprite;
    }
    public void OnAfterDeserialize()
    {

    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        switch(tag)
        {
            case "EnemyController":
                rb.velocity = Vector2.zero;
                break;
        }
    }
}
