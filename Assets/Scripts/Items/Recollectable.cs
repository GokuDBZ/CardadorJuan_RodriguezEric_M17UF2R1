using UnityEngine;
public class Recollectable : MonoBehaviour , ISerializationCallbackReceiver
{
    public WeaponSO wp;
    public int price;
    public WeaponSO Pick()
    {
        Destroy(gameObject);
        return wp;
    }
    public void OnBeforeSerialize()
    {
        GetComponent<SpriteRenderer>().sprite = wp.highlightedSprite;
    }
    public void OnAfterDeserialize()
    {

    }
}
