using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class EnemyController : MonoBehaviour
{
    public bool cleared;
    //[SerializeField] private GameObject dungeonManager;
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private List<EnemySO> enemySO;
    [SerializeField] private List<GameObject> roomEnemies;
    [SerializeField] private GameObject keyPreFab;
    public float spawned;
    public float clearedTime;
    private float clock;
    private void Start()
    {
        spawned = 0;
        cleared = false;
        clock = Time.time;
    }
    private void Update()
    {
        if(Time.time - clock > 0.3f)
        {
            clock = Time.time;

            if (spawned == 1)
            {
                cleared = GetComponentsInChildren<Enemy>().Count() == 0;
                if (cleared && clearedTime == 0) clearedTime = Time.time;
                if (cleared && Time.time - clearedTime > 30 && transform.parent.GetComponentsInChildren<Transform>().Where(item => { return item.name.Contains("Door") && item.GetComponent<BoxCollider2D>().enabled; }).Count() > 0)
                {
                    GameObject.Find("DungeonManager").GetComponent<DungeonManager>().OpenDoors(Convert.ToInt32(GameManager.instance.actualRoom.Split(' ')[1].Split(',')[0]), Convert.ToInt32(GameManager.instance.actualRoom.Split(' ')[2].Split(')')[0]), true);
                }
            }

            if (spawned > 1) spawned--;
        }
    }

    public void Spawn()
    {
        if(spawned == 0)
        {
            GameObject.Find("DungeonManager").GetComponent<DungeonManager>().OpenDoors(Convert.ToInt32(GameManager.instance.actualRoom.Split(' ')[1].Split(',')[0]), Convert.ToInt32(GameManager.instance.actualRoom.Split(' ')[2].Split(')')[0]), false);

            System.Random rnd = new();
            float num = (float)rnd.NextDouble();

            if (num >= 0 && num < 0.2) num = 1;
            else if (num >= 0.2 && num < 0.45) num = 2;
            else if (num >= 0.45 && num < 0.8) num = 3;
            else if (num >= 0.8 && num < 0.9) num = 4;
            else if (num >= 0.9 && num < 0.99) num = 5;
            else if (num >= 0.99 && num <= 1) num = 6;

            for (int i = 0; i < num; i++)
            {
                GameObject en = Instantiate(enemyPrefab, transform);
                en.transform.position = new Vector3((float)(rnd.NextDouble() * (6 - -6) + -6) + transform.position.x, (float)(rnd.NextDouble() * (2 - -4) + -4) + transform.position.y, -2);
                en.GetComponent<Enemy>().enemySO = enemySO[UnityEngine.Random.Range(0, enemySO.Count)];
                roomEnemies.Add(en);
            }
            spawned = 10;
        }
    }
    public bool TryKey(out GameObject key)
    {
        key = keyPreFab;
        return transform.GetComponentsInChildren<Transform>().Where(item => item.CompareTag("Enemy")).Count() == 1;
    }

    public void Freeze()
    {
        // Pausar a todos los enemigos
    }
    public void UnFreeze()
    {
        // Reanudar actividad de todos los enemigos
    }
}
