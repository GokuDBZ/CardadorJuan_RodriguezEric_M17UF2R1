using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DungeonManager : Entity
{
    [SerializeField] private GameObject roomPreFab;
    [SerializeField] private GameObject firstRoomPreFab;
    [SerializeField] private List<GameObject> rooms;
    [SerializeField] private List<GameObject> noDoors;
    [SerializeField] private GameObject newGameStairs;
    [Range(0,10)]
    [SerializeField] private int maxRoomsRow;
    public int finalRoomIndex;    Transform[] finalDoor;
    private List<int> createdIndex;
    private void Start()
    {
        finalDoor = new Transform[2];
        createdIndex = new List<int>();
        RoomGeneration();
        GenerateFinalRoom();
        Instantiate(GameManager.instance.player);
    }
    private void Update()
    {
        List<GameObject> rooms2 = rooms.Where(item => item != null).ToList();
        rooms2 = rooms2.Where(item => item.GetComponentInChildren<EnemyController>() != null).ToList();

        List<GameObject> rooms3 = rooms2.Where(item => item.GetComponentInChildren<EnemyController>().cleared).ToList();

        foreach (var item in finalDoor)
        {
            OpenDoor(item, rooms3.Count >= rooms2.Count);
        }
    }
    private void RoomGeneration()
    {
        for (int i = 0; i < maxRoomsRow * maxRoomsRow; i++)
        {
            rooms.Add(null);
        }
        
        if(maxRoomsRow % 2 == 1) GenerateRoom(maxRoomsRow * maxRoomsRow / 2);
        else GenerateRoom(maxRoomsRow * maxRoomsRow / 2 + maxRoomsRow / 2);
        
        while (rooms.Where(item => item != null).Count() < Mathf.Pow(maxRoomsRow, 2) * 0.4)
        {
            int index = 0;
            List<char> voids = new();
            for (int i = 4; i > 0;)
            {
                if (createdIndex.Where(item => RoomVoidsList(item, false).Count == i).Count() > 0)
                {
                    index = createdIndex.Where(item => RoomVoidsList(item, false).Count == i).ToArray()[0];
                    voids = RoomVoidsList(index, false);
                    i = 0;
                    break;
                }
                i--;
            }
            int doorsCount = -1;
            int tempIndex = index;
            foreach (var coor in voids)
            {
                switch (coor)
                {
                    case 'N':
                        if(RoomVoidsList(index - maxRoomsRow, false).Count > doorsCount ||
                            (RoomVoidsList(index - maxRoomsRow, false).Count == doorsCount && UnityEngine.Random.Range(0, 2) == 1))
                        {
                            doorsCount = RoomVoidsList(index - maxRoomsRow, false).Count;
                            tempIndex = index - maxRoomsRow;
                        }
                        break;
                    case 'S':
                        if (RoomVoidsList(index + maxRoomsRow, false).Count > doorsCount ||
                            (RoomVoidsList(index + maxRoomsRow, false).Count == doorsCount && UnityEngine.Random.Range(0, 2) == 1))
                        {
                            doorsCount = RoomVoidsList(index + maxRoomsRow, false).Count;
                            tempIndex = index + maxRoomsRow;
                        }
                        break;
                    case 'W':
                        if (RoomVoidsList(index - 1, false).Count > doorsCount ||
                            (RoomVoidsList(index - 1, false).Count == doorsCount && UnityEngine.Random.Range(0, 2) == 1))
                        {
                            doorsCount = RoomVoidsList(index - 1, false).Count;
                            tempIndex = index - 1;
                        }
                        break;
                    case 'E':
                        if (RoomVoidsList(index + 1, false).Count > doorsCount ||
                            (RoomVoidsList(index + 1, false).Count == doorsCount && UnityEngine.Random.Range(0, 2) == 1))
                        {
                            doorsCount = RoomVoidsList(index + 1, false).Count;
                            tempIndex = index + 1;
                        }
                        break;
                }
            }
            index = tempIndex;

            GenerateRoom(index);
        }
        for (int i = 0; i < rooms.Count; i++)
        {
            if(rooms[i] != null)
            {
                BlockDoors(i);
            }
        }
    }
    private void GenerateRoom(int index)
    {
        if(rooms.Where(item => item != null).Count() == 0) rooms[index] = Instantiate(firstRoomPreFab, transform);
        else rooms[index] = Instantiate(roomPreFab, transform);

        rooms[index].name = $"Room( {index % maxRoomsRow}, {index / maxRoomsRow})";
        if (maxRoomsRow % 2 == 1) rooms[index].transform.position = new Vector2((index % maxRoomsRow - maxRoomsRow / 2) * 20, (maxRoomsRow / 2 - index / maxRoomsRow) * 12);
        else rooms[index].transform.position = new Vector2((index % maxRoomsRow - maxRoomsRow / 2) * 20, (maxRoomsRow / 2 - index / maxRoomsRow) * 12);
        createdIndex.Add(index);
    }
    List<char> RoomVoidsList(int index, bool countOutLimit)
    {
        List<char> voids = new();
        if (countOutLimit)
        {
            if (index - maxRoomsRow < 0 || rooms[index - maxRoomsRow] == null) voids.Add('N');
            if (index + maxRoomsRow >= maxRoomsRow * maxRoomsRow || rooms[index + maxRoomsRow] == null) voids.Add('S');
            if (index % maxRoomsRow == 0 || index % maxRoomsRow != 0 && rooms[index - 1] == null) voids.Add('W');
            if (index % maxRoomsRow == maxRoomsRow - 1 || index % maxRoomsRow != maxRoomsRow - 1 && rooms[index + 1] == null) voids.Add('E');
            return voids;
        }
        if (index - maxRoomsRow >= 0 && rooms[index - maxRoomsRow] == null) voids.Add('N');
        if (index + maxRoomsRow < maxRoomsRow * maxRoomsRow && rooms[index + maxRoomsRow] == null) voids.Add('S');
        if (index - 1 % maxRoomsRow != maxRoomsRow && index % maxRoomsRow != 0 && rooms[index - 1] == null) voids.Add('W');
        if (index + 1 < maxRoomsRow * maxRoomsRow && index % maxRoomsRow != maxRoomsRow - 1 && rooms[index + 1] == null) voids.Add('E');
        return voids;
    }
    private void GenerateFinalRoom()
    {
        List<int> candidates = createdIndex.Where(item => RoomVoidsList(item, true).Count == 3).ToList();
        if (candidates.Count == 0) candidates = createdIndex.Where(item => RoomVoidsList(item, true).Count == 2).ToList();

        finalRoomIndex = candidates[Random.Range(0, candidates.Count)];

        Instantiate(newGameStairs, rooms[finalRoomIndex].transform);
        rooms[finalRoomIndex].transform.Find("EnemyController").gameObject.SetActive(false);

        RemoveDoors(finalRoomIndex, true, out char door);
        finalDoor[0] = rooms[finalRoomIndex].transform.Find("Door_" + door);
        switch (door)
        {
            case 'N':
                finalDoor[1] = rooms[finalRoomIndex - maxRoomsRow].transform.Find("Door_S");
                break;
            case 'S':
                finalDoor[1] = rooms[finalRoomIndex + maxRoomsRow].transform.Find("Door_N");
                break;
            case 'W':
                finalDoor[1] = rooms[finalRoomIndex - 1].transform.Find("Door_E");
                break;
            case 'E':
                finalDoor[1] = rooms[finalRoomIndex + 1].transform.Find("Door_W");
                break;
        }
        finalDoor[0].GetComponent<SpriteRenderer>().color = Color.red;
        finalDoor[1].GetComponent<SpriteRenderer>().color = Color.red;
    }

    private void BlockDoors(int index)
    {
        if (index / maxRoomsRow == 0 || rooms[index - maxRoomsRow] == null) RemoveDoor(rooms[index].transform.Find("Door_N"), false);
        if (index / maxRoomsRow == maxRoomsRow - 1 || rooms[index + maxRoomsRow] == null) RemoveDoor(rooms[index].transform.Find("Door_S"), false);

        if (index % maxRoomsRow == 0 || rooms[index - 1] == null) RemoveDoor(rooms[index].transform.Find("Door_W"), false);
        if (index % maxRoomsRow == maxRoomsRow - 1 || rooms[index + 1] == null) RemoveDoor(rooms[index].transform.Find("Door_E"), false);
    }
    private void RemoveDoors(int index, bool oneDoorLeft, out char door)
    {
        SpriteRenderer[] childs = rooms[index].transform.GetComponentsInChildren<SpriteRenderer>().Where(item => item.name[..4] == "Door").ToArray();
        int opennedDoor = -1;
        door = ' ';

        if(oneDoorLeft) opennedDoor = Random.Range(0, childs.Length - 1);

        for (int i = 0; i < childs.Length; i++)
        {
            if (i != opennedDoor)
            {
                RemoveDoor(childs[i].transform, true);
            }
            else door = childs[i].name.Split('_')[1].ToCharArray()[0];
        }
    }
    private void RemoveDoor(Transform door, bool withPairDoor)
    {
        GameObject walledDoor = null, pairDoor = null;
        GameObject wall = noDoors.Find(nodoor => nodoor.name == "Wall_" + door.name[5]);
        int index = rooms.IndexOf(door.parent.gameObject);
        
        if(withPairDoor)
        {
            switch (door.name[5])
            {
                case 'W':
                    if (index % maxRoomsRow > 0) pairDoor = rooms[index - 1].GetComponentsInChildren<Transform>().Where(item => item.name == "Door_E").First().gameObject;
                    break;
                case 'E':
                    if (index % maxRoomsRow < maxRoomsRow - 1) pairDoor = rooms[index + 1].GetComponentsInChildren<Transform>().Where(item => item.name == "Door_W").First().gameObject;
                    break;
                case 'S':
                    if (index / maxRoomsRow < maxRoomsRow - 1) pairDoor = rooms[index + maxRoomsRow].GetComponentsInChildren<Transform>().Where(item => item.name == "Door_N").First().gameObject;
                    break;
                case 'N':
                    if (index / maxRoomsRow > 0) pairDoor = rooms[index - maxRoomsRow].GetComponentsInChildren<Transform>().Where(item => item.name == "Door_S").First().gameObject;
                    break;
            }
            if (pairDoor != null)
            {
                GameObject walledPairDoor = Instantiate(noDoors.Find(nodoor => nodoor.name == "NoDoor_" + pairDoor.name[5]), pairDoor.transform.parent);
                walledPairDoor.transform.position = new Vector3(pairDoor.transform.position.x, pairDoor.transform.position.y, -2);
                pairDoor.SetActive(false);
            }
        }
        walledDoor = Instantiate(wall, rooms[index].transform);
        walledDoor.transform.position = new Vector3(door.position.x, Mathf.Round(door.position.y - 1 * System.Convert.ToInt32(door.name[5] != 'S')), -1);
        door.gameObject.SetActive(false);
    }
    public void OpenDoors(int x, int y, bool open)
    {
        int index = x + y * maxRoomsRow;
        foreach (SpriteRenderer door in rooms[index].transform.GetComponentsInChildren<SpriteRenderer>().Where(item => item.name[..4] == "Door"))
        {
            OpenDoor(door.transform, open);
            switch (door.name[5])
            {
                case 'W':
                    OpenDoor(rooms[index - 1].transform.Find("Door_E"), open);
                    break;
                case 'E':
                    OpenDoor(rooms[index + 1].transform.Find("Door_W"), open);
                    break;
                case 'S':
                    OpenDoor(rooms[index + maxRoomsRow].transform.Find("Door_N"), open);
                    break;
                case 'N':
                    OpenDoor(rooms[index - maxRoomsRow].transform.Find("Door_S"), open);
                    break;
            }
        }
    }
    private void OpenDoor(Transform door, bool open)
    {
        if (door.name != "Door_S") door.GetComponent<Door>().Use(open);
        door.GetComponent<BoxCollider2D>().enabled = !open;
    }
}
