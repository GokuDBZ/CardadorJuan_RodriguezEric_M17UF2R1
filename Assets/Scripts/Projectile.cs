using UnityEngine;

public class Projectile : Entity
{
    public bool destroyOnWall;
    public Transform firePoint;
    public float damage;
    public float knockback;
    private float explosionDamage;
    private bool exploded;
    private float clock;
    public Vector2 dot;
    private void Start()
    {
        clock = Time.time;
        destroyOnWall = true;
        float collisionRadius = 0;
        switch(name)
        {
            case "Melee_Projectile":
                collisionRadius = 0.3f;
                break;
            case "Sniper_Projectile":
            case "Turret_Projectile":
                collisionRadius = 0.25f;
                break;
            case "FlameThrower_Projectile":
                collisionRadius = 0.2f;
                break;
            case "GrenadeLauncher_Projectile":
                anim.SetFloat("Explosion", -1);
                explosionDamage = damage;
                damage = 0;
                exploded = false;
                collisionRadius = 0.3f;
                GetComponent<CircleCollider2D>().isTrigger = false;
                break;
        }
        GetComponent<CircleCollider2D>().radius = collisionRadius;
    }
    void FixedUpdate()
    {
        if (Time.time - clock > 0.1f)
        {
            clock = Time.time;

            if (name.Contains("GrenadeLauncher"))
            {
                if (anim.GetFloat("Explosion") > 0)
                {
                    anim.SetFloat("Explosion", anim.GetFloat("Explosion") - 1);
                }
            }
        }
        if(name.Contains("FlameThrower"))
        {
            transform.localScale *= 1.05f;
        }
        if(name.Contains("GrenadeLauncher"))
        {
            rb.velocity *= 0.975f;
            if (Mathf.Abs(rb.velocity.y) + Mathf.Abs(rb.velocity.x) < 1) rb.velocity = Vector2.zero;
            if (Mathf.Abs(rb.velocity.y) < 1f && Mathf.Abs(rb.velocity.x) < 1f)
            {
                if(!exploded)
                {
                    exploded = true;
                    foreach (var item in Physics2D.OverlapCircleAll(transform.position, 2))
                    {
                        item.TryGetComponent(out Enemy a);
                        if(a != null)
                        {
                            a.TakeDamage(explosionDamage);
                        }
                    }
                }
                if (anim.GetFloat("Explosion") < 0)
                {
                    anim.SetFloat("Explosion", 10);
                    SoundManager.instance.Play("Explosion");
                }
                GetComponent<CircleCollider2D>().isTrigger = true;
                transform.localScale *= 1.5f;
                if (transform.localScale.x > 10)
                {
                    if (anim.GetFloat("Explosion") == 0)
                    {
                        sr.color = Color.red;
                        //Destroy(gameObject);
                    }
                } 
            }
        }
        if(name.Contains("Melee"))
        {
            transform.position = firePoint.position;
        }
        transform.position = new Vector3(transform.position.x, transform.position.y, -2);
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("Walls") && name != "Melee_Projectile") Destroy(gameObject);

        if (((col.CompareTag("Player") && !tag.Contains("Player")) ||
            (col.CompareTag("Enemy") && !tag.Contains("Enemy"))) &&
            !name.Contains("GrenadeLauncher") &&
            !name.Contains("Melee")) Destroy(gameObject);

        if (tag.Contains("Enemy") && col.name.Contains("Melee")) Destroy(gameObject);

    }
    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("MainCamera")) Destroy(gameObject);
    }
}
