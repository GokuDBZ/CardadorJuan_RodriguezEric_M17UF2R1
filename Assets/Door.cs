using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] private Sprite openned;
    [SerializeField] private Sprite closed;
    public void Use(bool open)
    {
        if(open)
        {
            GetComponent<SpriteRenderer>().sprite = openned;
            transform.position = new Vector3(transform.position.x, transform.position.y,-3);
            return;
        }
        GetComponent<SpriteRenderer>().sprite = closed;
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
    }
}
