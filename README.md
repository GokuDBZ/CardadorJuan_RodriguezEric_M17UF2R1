# Mago Culiqo

### Instrucciones
- Movimiento = W/A/S/D
- Cambiar de arma = Q/E
- Disparar = Click izquierdo
- Usar pocion = Click derecho
- Volver al menu principal = ESC

### Descripcion
- Mago Culiqo es un roguelike,  en el que controlamos a un mago en busca de riquezas que decide entrar a una mazmorra para encontrar tesoros.
- Tiene un estilo topdown pixel art de 32x32
